(defproject jdbam "0.0.1-SNAPSHOT"
  :description "Clojure JDBM binding"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.apache.jdbm/jdbm "3.0-alpha4"]])
