(ns jdbam.core
  (:import [org.apache.jdbm DBMaker]))

(defn open-db [file & {:keys [close-on-exit] :or {:close-on-exit true}}]
  (let [maker (DBMaker/openFile file)
        maker (if close-on-exit
                (.closeOnExit maker)
                maker)]
    (.make maker)))

(defn get-collection [db coll]
  (let [coll (name coll)]
    {:db db
     :map (or (.getTreeMap db coll)
              (.createTreeMap db coll))}))

(defmacro with-transaction [db & body]
  `(let [db# ~db]
     (try
       (when (do ~@body)
         (.commit db#)
         true)
       (catch Throwable e#
         (.rollback db#)
         (throw e#)))))

(defn put! [coll key value]
  (with-transaction (:db coll)
    (.put (:map coll) (name key) (pr-str value))
    true))

(defn fetch [coll key & [not-found]]
  (if-let [value (.get (:map coll) (name key))]
    (read-string value)
    not-found))

(defn fetch-keys [coll]
  (set (.keySet (:map coll))))

(defn fetch-map [coll]
  (->> (fetch-keys coll)
       (map (juxt identity (partial fetch coll)))
       (into {})))

(defn update! [coll key update & args]
  (let [key (name key)
        coll*  (:map coll)
        update (fn [value]
                 (pr-str (apply update value args)))
        value  (or (.get coll* key)
                   (.putIfAbsent coll* key (update nil)))]
    (when value
      (loop [value (read-string value)]
        (or (with-transaction (:db coll)
              (.replace coll* key (pr-str value) (update value)))
            (recur (fetch coll key)))))))
